from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        # fields = "__all__"
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        )


class CategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        # fields = "__all__"
        fields = ("name",)


class AccountForm(ModelForm):
    class Meta:
        model = Account
        # fields = "__all__"
        fields = (
            "name",
            "number",
        )
